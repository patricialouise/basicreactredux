import React from 'react'
import Post from './Post'

const PostList = ({ posts }) => (
    <ul>
        {posts.map(post =>
            <Post {...post} />
        )}
    </ul>
)

export default PostList
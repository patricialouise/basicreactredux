import React from 'react'
import PostListContainer from '../containers/PostListContainer'

const App = () => (
    <div>
       <PostListContainer />
    </div>
)

export default App
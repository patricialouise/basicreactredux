export const initialState = {
    posts: [
        {text: "Hello"},
        {text: "World"}
     ] // Replace with empty and then initialize in index.js
}

export const PostActionTypes = {
    LOADED_MORE : "LOADED_MORE"
}

// Action Creators
export const loadMore = () => {
    // Replace with Axios API Call
    var fetchedPosts = ["Fetch", "Me"]
    
    return {
        type: PostActionTypes.LOADED_MORE,
        loadedPosts: fetchedPosts
    }
}
import { connect } from 'react-redux'
import PostList from '../components/PostList'

// Map state.posts to posts so that PostList can see receive it
const mapStateToProps = state => {
    console.log(state) // observe in Chrome's F12 Console and you'll notice what the state object is
    return {
        posts: state.posts.posts // see console.log above for explanation
    }
}

// No mapping needed, we don't need a dispatch in PostList anyway
const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostList)
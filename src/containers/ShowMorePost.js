import React from 'react'
import { connect } from 'react-redux'
import { loadMore } from '../actions/postActions'

const ShowMorePost = ({dispatch}) => {
    return (
        <div>
            <form onSubmit= { e => {
                e.preventDefault()
                dispatch(loadMore())
            }}
            >
                <button type="submit">
                    Load more
                </button>
            </form>
        </div>
    )
}

export default connect()(ShowMorePost)
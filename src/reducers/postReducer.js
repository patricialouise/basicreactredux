import { PostActionTypes } from '../actions/postActions'
import { initialState } from '../actions/postActions'

const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case PostActionTypes.LOADED_MORE:
            return {
                ...state,
                posts: [...state.posts, action.loadedPosts]
            }
        default:
            return state
    }
}

export default postReducer
import { combineReducers } from 'redux'
import postReducer from './postReducer'

export default combineReducers({
    posts: postReducer // Makes the state under this reducer to start with 'posts'
})